import Vue from 'vue'
import Vuetify from 'vuetify'
import VueI18n from 'vue-i18n'

Vue
  .use(VueI18n)
  .use(Vuetify)
Vue.config.productionTip = false
